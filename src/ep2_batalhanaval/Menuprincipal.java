package ep2_batalhanaval;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import canvas.Canvasjogo;
import canvas.Interface;

import javax.swing.JTextField;
import java.awt.Font;
import java.awt.Color;
import javax.swing.DropMode;
import javax.swing.JButton;
import javax.swing.SwingConstants;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JRadioButton;
import java.awt.GridLayout;
import net.miginfocom.swing.MigLayout;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;

@SuppressWarnings("serial")
public class Menuprincipal extends JFrame {

	private JPanel contentPane;
	private JTextField txtLucasGanda;

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Menuprincipal frame = new Menuprincipal();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	public Menuprincipal() {
		setBackground(new Color(192, 192, 192));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 600, 600);
		contentPane = new JPanel();
		contentPane.setBorder(null);
		contentPane.setForeground(Color.PINK);
		contentPane.setBackground(Color.pink);
		setContentPane(contentPane);
		GridBagLayout gbl_contentPane = new GridBagLayout();
		gbl_contentPane.columnWidths = new int[]{600, 0};
		gbl_contentPane.rowHeights = new int[]{566, 34, 0};
		gbl_contentPane.columnWeights = new double[]{0.0, Double.MIN_VALUE};
		gbl_contentPane.rowWeights = new double[]{0.0, 0.0, Double.MIN_VALUE};
		contentPane.setLayout(gbl_contentPane);
		
		JButton jogar = new JButton("JOGAR");
		jogar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(e.getSource()==jogar);
				{
					Canvasjogo canvas = new Canvasjogo();
					canvas.setVisible(true);
					Interface frame = new Interface();
					frame.setVisible(true);
					dispose();
				}
			}
		});
		GridBagConstraints gbc_jogar = new GridBagConstraints();
		gbc_jogar.insets = new Insets(0, 0, 5, 0);
		gbc_jogar.gridx = 0;
		gbc_jogar.gridy = 0;
		contentPane.add(jogar, gbc_jogar);
		
		txtLucasGanda = new JTextField();
		txtLucasGanda.setHorizontalAlignment(SwingConstants.LEFT);
		txtLucasGanda.setEditable(false);
		txtLucasGanda.setText("Lucas Ganda - 170039668");
		txtLucasGanda.setBackground(Color.PINK);
		txtLucasGanda.setFont(new Font("NanumMyeongjo", Font.BOLD, 26));
		GridBagConstraints gbc_txtLucasGanda = new GridBagConstraints();
		gbc_txtLucasGanda.anchor = GridBagConstraints.NORTH;
		gbc_txtLucasGanda.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtLucasGanda.gridx = 0;
		gbc_txtLucasGanda.gridy = 1;
		contentPane.add(txtLucasGanda, gbc_txtLucasGanda);
		txtLucasGanda.setColumns(10);
	}

}
