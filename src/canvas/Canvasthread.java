package canvas;

import java.awt.Graphics;

public class Canvasthread extends Thread {
	private Canvasjogo canvas;
	private boolean running = true; 
	
	public Canvasthread(Canvasjogo canvas) {
		this.canvas = canvas;
	}
	
	@Override
	public void run() {
		while(running) {
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			canvas.paint(canvas.getGraphics());
		}
	}
}
